﻿using System;
using System.Buffers.Binary;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Syroot.BinaryData.Core;

namespace Syroot.BinaryData.Memory
{
    /// <summary>
    /// Represents a cursor in a <see cref="ReadOnlySpan{Byte}"/> at which data is read from.
    /// </summary>
    [DebuggerDisplay(nameof(SpanReader) + ", Position={Position}")]
    public ref struct SpanReader
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _reverseEndian;
        private Encoding _encoding;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="SpanReader"/> struct to operate on the given
        /// <paramref name="span"/>.
        /// </summary>
        /// <param name="span">The <see cref="ReadOnlySpan{Byte}"/> to read from.</param>
        /// <param name="endian">The <see cref="Endian"/> to use.</param>
        /// <param name="encoding">The character encoding to use. Defaults to <see cref="Encoding.UTF8"/>.</param>
        public SpanReader(ReadOnlySpan<byte> span, Endian endian = Endian.System, Encoding encoding = null)
        {
            _reverseEndian = !endian.IsSystem();
            _encoding = encoding ?? Encoding.UTF8;
            Position = 0;
            Span = span;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the encoding used for string related operations where no other encoding has been provided.
        /// Setting this value to <c>null</c> will restore the default <see cref="Encoding.UTF8"/>.
        /// </summary>
        public Encoding Encoding
        {
            get => _encoding;
            set => _encoding = value ?? Encoding.UTF8;
        }

        /// <summary>
        /// Gets or sets the <see cref="Endian"/> used to parse multibyte binary data with.
        /// </summary>
        public Endian Endian
        {
            get => _reverseEndian ? EndianTools.NonSystemEndian : EndianTools.SystemEndian;
            set => _reverseEndian = !value.IsSystem();
        }

        /// <summary>
        /// Gets a value indicating whether the current position reached the end of the span.
        /// </summary>
        public bool IsEndOfSpan => Span.Length - Position <= 0;

        /// <summary>
        /// Gets the length of the underlying <see cref="Span"/>.
        /// </summary>
        public int Length => Span.Length;

        /// <summary>
        /// Gets or sets the index in the <see cref="Span"/> at which the next data is read from.
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// Gets the <see cref="ReadOnlySpan{Byte}"/> from which data is read.
        /// </summary>
        public ReadOnlySpan<byte> Span { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Aligns the position to the given byte multiple.
        /// </summary>
        /// <param name="alignment">The byte multiple to align to. If negative, the position is decreased to the
        /// previous multiple rather than the next one.</param>
        /// <returns>The new position.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int Align(int alignment) => Position += MathTools.GetAlignmentDelta(Position, alignment);

        /// <summary>
        /// Reads a 1-byte <see cref="Boolean"/> value from the current position. This is the .NET default format.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Boolean ReadBoolean() => Span[Position++] != 0;

        /// <summary>
        /// Reads a 2-byte <see cref="Boolean"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Boolean ReadBoolean2()
        {
            UInt16 value = MemoryMarshal.Read<UInt16>(Span.Slice(Position));
            Position += sizeof(UInt16);
            return value != 0;
        }

        /// <summary>
        /// Reads a 4-byte <see cref="Boolean"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Boolean ReadBoolean4()
        {
            UInt32 value = MemoryMarshal.Read<UInt32>(Span.Slice(Position));
            Position += sizeof(UInt32);
            return value != 0;
        }

        /// <summary>
        /// Reads the given number of <see cref="Byte"/> values from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Byte[] ReadBytes(int count)
        {
            byte[] value = Span.Slice(Position, count).ToArray();
            Position += count;
            return value;
        }

        /// <summary>
        /// Reads a <see cref="Byte"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Byte ReadByte() => Span[Position++];

        /// <summary>
        /// Reads a <see cref="DateTime"/> value from the current position, stored as the ticks of a .NET
        /// <see cref="DateTime"/> instance.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public DateTime ReadDateTime() => new DateTime(ReadInt64());

        /// <summary>
        /// Reads a <see cref="DateTime"/> value from the current position, stored in the 32-bit time_t format of the C
        /// library. This is a <see cref="UInt32"/> which can store the seconds from 1970-01-01 until approx.
        /// 2106-02-07.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public DateTime ReadDateTimeT() => CTimeTools.GetDateTime(ReadUInt32());

        /// <summary>
        /// Reads a <see cref="DateTime"/> value from the current position, stored in the 64-bit time_t format of the C
        /// library. This is an <see cref="Int64"/> which can store the seconds from 1970-01-01 until approx.
        /// 292277026596-12-04.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public DateTime ReadDateTimeT64() => CTimeTools.GetDateTime(ReadUInt64());

        /// <summary>
        /// Reads a <see cref="Decimal"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Decimal ReadDecimal() => MemoryMarshal.Read<Decimal>(Span.Slice(Position));

        /// <summary>
        /// Reads a <see cref="Double"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe Double ReadDouble()
        {
            UInt64 raw = ReadUInt64();
            return *(Double*)&raw;
        }

        /// <summary>
        /// Reads an <see cref="Enum"/> value from the current position, using the size of the underlying type.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T ReadEnum<T>() where T : struct, Enum
        {
            int length = Unsafe.SizeOf<T>();
            Span<byte> raw = stackalloc byte[length];
            Span.Slice(Position, length).CopyTo(raw);
            Position += length;
            if (_reverseEndian)
                raw.Reverse();
            return MemoryMarshal.Read<T>(raw);
        }

        /// <summary>
        /// Reads an <see cref="Enum"/> value from the current position, using the size of the underlying type. The
        /// value is validated, throwing an exception if it is not defined in the enum.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentException">The value is not defined in the enum.</exception>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T ReadEnumSafe<T>() where T : struct, Enum
        {
            T value = ReadEnum<T>();
            if (!EnumTools.Validate(typeof(T), value))
                throw new ArgumentException($"Value {value} is not valid for enum {typeof(T)}.");
            return value;
        }

        /// <summary>
        /// Reads an <see cref="Int16"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Int16 ReadInt16()
        {
            Int16 value = MemoryMarshal.Read<Int16>(Span.Slice(Position));
            Position += sizeof(Int16);
            return _reverseEndian ? BinaryPrimitives.ReverseEndianness(value) : value;
        }

        /// <summary>
        /// Reads an <see cref="Int32"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Int32 ReadInt32()
        {
            Int32 value = MemoryMarshal.Read<Int32>(Span.Slice(Position));
            Position += sizeof(Int32);
            return _reverseEndian ? BinaryPrimitives.ReverseEndianness(value) : value;
        }

        /// <summary>
        /// Reads an <see cref="Int32"/> value from the current position. The value is stored in 1 to 5 bytes, only
        /// using another byte if it does not fit into 7 more bits of the current one.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentException">The data available is not a 7-bit encoded integer.</exception>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Int32 ReadInt32Bit7()
        {
            // Endianness does not matter, as this value is stored byte by byte.
            // While the highest bit is set, the integer requires another of a maximum of 5 bytes.
            Int32 value = 0;
            for (int i = 0; i < sizeof(Int32) + 1; i++)
            {
                byte readByte = Span[Position++];
                value |= (readByte & 0b01111111) << i * 7;
                if ((readByte & 0b10000000) == 0)
                    return value;
            }
            throw new ArgumentException("Invalid 7-bit encoded Int32.");
        }

        /// <summary>
        /// Reads an <see cref="Int64"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Int64 ReadInt64()
        {
            Int64 value = MemoryMarshal.Read<Int64>(Span.Slice(Position));
            Position += sizeof(Int64);
            return _reverseEndian ? BinaryPrimitives.ReverseEndianness(value) : value;
        }

        /// <summary>
        /// Reads an <see cref="SByte"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public SByte ReadSByte() => (SByte)Span[Position++];

        /// <summary>
        /// Reads an <see cref="Single"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe Single ReadSingle()
        {
            UInt32 raw = ReadUInt32();
            return *(Single*)&raw;
        }

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has a prefix of a 7-bit encoded integer of
        /// variable size determining the number of bytes out of which the string consists, and no postfix. This is the
        /// .NET <see cref="System.IO.BinaryReader"/> and <see cref="System.IO.BinaryWriter"/> default format.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadString()
        {
            int length = ReadInt32Bit7();
#if NETCOREAPP2_1
            String value = _encoding.GetString(Span.Slice(Position, length));
#else
            String value;
            unsafe
            {
                fixed (byte* pSpan = Span.Slice(Position))
                    value = _encoding.GetString(pSpan, length);
            }
#endif
            Position += length;
            return value;
        }

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has no prefix and is terminated with a 0
        /// value. The size of this value depends on the encoding.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadString0()
        {
            // Read byte or word values until a 0 value is found (no encoding's char surrogate should consist of 0).
            // Endianness depends on encoding, not the actual values.
            int terminatorSize = _encoding.GetByteCount("A");
            int length = 0;
            while (true)
            {
                // Check for zero bytes of the terminator.
                int i;
                for (i = 0; i < terminatorSize; i++)
                {
                    if (Span[Position + length + i] != 0)
                    {
                        length++;
                        break;
                    }
                }
                // If a terminator was detected, return the string up to it.
                if (i == terminatorSize)
                {
#if NETCOREAPP2_1
                    String value = _encoding.GetString(Span.Slice(Position, length));
#else
                    String value;
                    unsafe
                    {
                        fixed (byte* pSpan = Span.Slice(Position))
                            value = _encoding.GetString(pSpan, length);
                    }
#endif
                    Position += length + terminatorSize;
                    return value;
                }
            }
        }

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has a <see cref="Byte"/> prefix
        /// determining the number of chars out of which the string consists, and no postfix.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadString1() => DecodeString(ReadByte());

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has a <see cref="UInt16"/> prefix
        /// determining the number of chars out of which the string consists, and no postfix.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadString2() => DecodeString(ReadUInt16());

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has an <see cref="Int32"/> prefix
        /// determining the number of chars out of which the string consists, and no postfix.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadString4() => DecodeString(ReadInt32());

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has neither prefix nor postfix, and is
        /// stored in a buffer with the given <paramref name="byteCount"/>, padded with 0 if it is longer than the
        /// string data.
        /// </summary>
        /// <param name="byteCount">The number of bytes to read the value from.</param>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadStringFix(int byteCount)
        {
#if NETCOREAPP2_1
            String value = _encoding.GetString(Span.Slice(Position, byteCount));
#else
            String value;
            unsafe
            {
                fixed (byte* pSpan = Span.Slice(Position))
                    value = _encoding.GetString(pSpan, byteCount);
            }
#endif
            Position += byteCount;
            return value.TrimEnd('\0');
        }

        /// <summary>
        /// Reads a <see cref="String"/> value from the current position. It has neither prefix nor postfix, the given
        /// <paramref name="length"/> is expected.
        /// </summary>
        /// <param name="length">The length of the string, in characters.</param>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public String ReadStringRaw(int length) => DecodeString(length);

        /// <summary>
        /// Reads a <see cref="UInt16"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public UInt16 ReadUInt16()
        {
            UInt16 value = MemoryMarshal.Read<UInt16>(Span.Slice(Position));
            Position += sizeof(UInt16);
            return _reverseEndian ? BinaryPrimitives.ReverseEndianness(value) : value;
        }

        /// <summary>
        /// Reads a <see cref="UInt32"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public UInt32 ReadUInt32()
        {
            UInt32 value = MemoryMarshal.Read<UInt32>(Span.Slice(Position));
            Position += sizeof(UInt32);
            return _reverseEndian ? BinaryPrimitives.ReverseEndianness(value) : value;
        }

        /// <summary>
        /// Reads a <see cref="UInt64"/> value from the current position.
        /// </summary>
        /// <returns>The value retrieved from the current position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">There are less bytes available than required.</exception>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public UInt64 ReadUInt64()
        {
            UInt64 value = MemoryMarshal.Read<UInt64>(Span.Slice(Position));
            Position += sizeof(UInt64);
            return _reverseEndian ? BinaryPrimitives.ReverseEndianness(value) : value;
        }

        /// <summary>
        /// Gets a <see cref="SpanReader"/> for the remaining data following the current position.
        /// </summary>
        /// <returns>The <see cref="SpanReader"/> covering the remaining data.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public SpanReader Slice() => new SpanReader(Span.Slice(Position));

        /// <summary>
        /// Gets a <see cref="SpanReader"/> for the data covered from the given <paramref name="start"/> position.
        /// </summary>
        /// <param name="start">The position from which to start covering data.</param>
        /// <returns>The <see cref="SpanReader"/> covering the remaining data.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public SpanReader Slice(int start) => new SpanReader(Span.Slice(start));

        /// <summary>
        /// Gets a <see cref="SpanReader"/> for the data covered by the given <paramref name="start"/> position and
        /// <paramref name="length"/>.
        /// </summary>
        /// <param name="start">The position from which to start covering data.</param>
        /// <param name="length">The number of bytes to cover.</param>
        /// <returns>The <see cref="SpanReader"/> covering the remaining data.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public SpanReader Slice(int start, int length) => new SpanReader(Span.Slice(start, length));

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private String DecodeString(int length)
        {
            if (length == 0)
                return String.Empty;

            Decoder decoder = _encoding.GetDecoder();
            Span<char> chars = stackalloc char[length];
#if NETCOREAPP2_1
            decoder.Convert(Span.Slice(Position), chars, true, out int bytesUsed, out int charsUsed,
                out bool completed);
#else
            int bytesUsed;
            unsafe
            {
                fixed (byte* pSpan = Span.Slice(Position))
                {
                    fixed (char* pChars = chars)
                    {
                        decoder.Convert(pSpan, Span.Length - Position, pChars, length, true, out bytesUsed,
                            out int charsUsed, out bool completed);
                    }
                }
            }
#endif
            Position += bytesUsed;
            return chars.ToString();
        }
    }
}
