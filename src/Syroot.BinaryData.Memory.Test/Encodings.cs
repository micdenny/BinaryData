﻿using System.Text;

namespace Syroot.BinaryData.Memory.Test
{
    internal static class Encodings
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal static readonly Encoding ShiftJIS;
        internal static readonly Encoding UHC;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static Encodings()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            ShiftJIS = Encoding.GetEncoding(932);
            UHC = Encoding.GetEncoding(949);
        }
    }
}
