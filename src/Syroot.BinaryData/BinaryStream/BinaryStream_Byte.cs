﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        // ---- Read 1 Byte ----

        /// <summary>
        /// Returns a <see cref="Byte"/> instance read from the underlying stream.
        /// </summary>
        /// <returns>The value read from the current stream.</returns>
        public Byte Read1Byte()
            => BaseStream.Read1Byte();

        /// <summary>
        /// Returns a <see cref="Byte"/> instance read asynchronously from the underlying stream.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The value read from the current stream.</returns>
        public async Task<byte> Read1ByteAsync(
            CancellationToken cancellationToken = default)
            => await BaseStream.Read1ByteAsync(cancellationToken);

        // ---- Read N Bytes ----

        /// <summary>
        /// Returns an array of <see cref="Byte"/> instances read from the underlying stream.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public Byte[] ReadBytes(int count)
            => BaseStream.ReadBytes(count);

        /// <summary>
        /// Returns an array of <see cref="Byte"/> instances read asynchronously from the underlying stream.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public async Task<Byte[]> ReadBytesAsync(int count,
            CancellationToken cancellationToken = default)
            => await BaseStream.ReadBytesAsync(count, cancellationToken);

        // ---- Write 1 Byte ----

        /// <summary>
        /// Writes a <see cref="Byte"/> value to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void Write(Byte value)
            => BaseStream.WriteByte(value); // use System.IO.Stream implementation directly.

        /// <summary>
        /// Writes a <see cref="Byte"/> value asynchronously to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(Byte value,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(value, cancellationToken);

        // public void WriteByte(Byte value) already implemented by System.IO.Stream

        /// <summary>
        /// Writes a <see cref="Byte"/> value asynchronously to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteByteAsync(Byte value,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(value, cancellationToken);

        // ---- Write N Bytes ----

        /// <summary>
        /// Writes an enumerable of <see cref="Byte"/> values to the underlying stream. This method writes bytes
        /// one-by-one.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void Write(IEnumerable<Byte> values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an enumerable of <see cref="Byte"/> values to the underlying stream. This method writes bytes
        /// one-by-one.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(IEnumerable<Byte> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);

        /// <summary>
        /// Writes an enumerable of <see cref="Byte"/> values to the underlying stream. This method writes bytes
        /// one-by-one.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void WriteBytes(IEnumerable<Byte> values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an enumerable of <see cref="Byte"/> values to the underlying stream. This method writes bytes
        /// one-by-one.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteBytesAsync(IEnumerable<Byte> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);

        // ---- Write N Bytes (1 call) ----

        /// <summary>
        /// Writes an array of <see cref="Byte"/> values to the underlying stream. This method writes all bytes
        /// in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void Write(Byte[] values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an array of <see cref="Byte"/> values to the underlying stream. This method writes all bytes
        /// in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(Byte[] values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);

        /// <summary>
        /// Writes an array of <see cref="Byte"/> values to the underlying stream. This method writes all bytes
        /// in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void WriteBytes(Byte[] values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an array of <see cref="Byte"/> values to the underlying stream. This method writes all bytes
        /// in one call.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteBytesAsync(Byte[] values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);
    }
}
