﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        // ---- Read 1 7BitInt32 ----

        /// <summary>
        /// Returns a variable-length <see cref="Int32"/> instance read from the given underlying stream which
        /// can require up to 5 bytes.
        /// </summary>
        /// <returns>The value read from the current stream.</returns>
        public Int32 Read7BitInt32()
            => BaseStream.Read7BitInt32();

        /// <summary>
        /// Returns a variable-length <see cref="Int32"/> instance read asynchronously from the given
        /// underlying stream which can require up to 5 bytes.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The value read from the current stream.</returns>
        public async Task<Int32> Read7BitInt32Async(
            CancellationToken cancellationToken = default)
            => await BaseStream.Read7BitInt32Async(cancellationToken);

        // ---- Read N 7BitInt32s ----

        /// <summary>
        /// Returns an array of variable-length <see cref="Int32"/> instances read from the underlying stream
        /// which can require to 5 bytes each.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public Int32[] Read7BitInt32s(int count)
            => BaseStream.Read7BitInt32s(count);

        /// <summary>
        /// Returns an array of variable-length <see cref="Int32"/> instances read asynchronously from the
        /// underlying stream which can require to 5 bytes each.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public async Task<Int32[]> Read7BitInt32sAsync(int count,
            CancellationToken cancellationToken = default)
            => await BaseStream.Read7BitInt32sAsync(count, cancellationToken);

        // ---- Write 1 7BitInt32 ----

        /// <summary>
        /// Writes a variable-length <see cref="Int32"/> value to the underlying stream which can require up to
        /// 5 bytes.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void Write7BitInt32(Int32 value)
            => BaseStream.Write7BitInt32(value);

        /// <summary>
        /// Writes a variable-length <see cref="Int32"/> value asynchronously to the underlying stream which can
        /// require up to 5 bytes.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task Write7BitInt32Async(Int32 value,
            CancellationToken cancellationToken = default)
            => await BaseStream.Write7BitInt32Async(value, cancellationToken);

        // ---- Write N 7BitInt32s ----

        /// <summary>
        /// Writes an enumerable of <see cref="Int32"/> values to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void Write7BitInt32s(IEnumerable<Int32> values)
            => BaseStream.Write7BitInt32s(values);

        /// <summary>
        /// Writes an enumerable of <see cref="Int32"/> values asynchronously to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task Write7BitInt32Async(IEnumerable<Int32> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.Write7BitInt32sAsync(values, cancellationToken);
    }
}
