﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.BinaryData
{
    public partial class BinaryStream
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        // ---- Read 1 SByte ----

        /// <summary>
        /// Returns an <see cref="SByte"/> instance read from the underlying stream.
        /// </summary>
        /// <returns>The value read from the current stream.</returns>
        public SByte ReadSByte()
            => BaseStream.ReadSByte();

        /// <summary>
        /// Returns an <see cref="SByte"/> instance read asynchronously from the underlying stream.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The value read from the current stream.</returns>
        public async Task<SByte> ReadSByteAsync(
            CancellationToken cancellationToken = default)
            => await BaseStream.ReadSByteAsync(cancellationToken);

        // ---- Read N SBytes ----

        /// <summary>
        /// Returns an array of <see cref="SByte"/> instances read from the underlying stream.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public SByte[] ReadSBytes(int count)
            => BaseStream.ReadSBytes(count);

        /// <summary>
        /// Returns an array of <see cref="SByte"/> instances read asynchronously from the underlying stream.
        /// </summary>
        /// <param name="count">The number of values to read.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>The array of values read from the current stream.</returns>
        public async Task<SByte[]> ReadSBytesAsync(int count,
            CancellationToken cancellationToken = default)
            => await BaseStream.ReadSBytesAsync(count, cancellationToken);

        // ---- Write 1 SByte ----

        /// <summary>
        /// Writes an <see cref="SByte"/> value to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void Write(SByte value)
            => BaseStream.Write(value);

        /// <summary>
        /// Writes an <see cref="SByte"/> value asynchronously to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(SByte value,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(value, cancellationToken);

        /// <summary>
        /// Writes an <see cref="SByte"/> value to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        public void WriteSByte(SByte value)
            => BaseStream.Write(value);

        /// <summary>
        /// Writes an <see cref="SByte"/> value asynchronously to the underlying stream.
        /// </summary>
        /// <param name="value">The value to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteSByteAsync(SByte value,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(value, cancellationToken);

        // ---- Write N SBytes ----

        /// <summary>
        /// Writes an enumerable of <see cref="SByte"/> values to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void Write(IEnumerable<SByte> values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an enumerable of <see cref="SByte"/> values asynchronously to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteAsync(IEnumerable<SByte> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);

        /// <summary>
        /// Writes an enumerable of <see cref="SByte"/> values to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        public void WriteSBytes(IEnumerable<SByte> values)
            => BaseStream.Write(values);

        /// <summary>
        /// Writes an enumerable of <see cref="SByte"/> values asynchronously to the underlying stream.
        /// </summary>
        /// <param name="values">The values to write.</param>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        public async Task WriteSBytesAsync(IEnumerable<SByte> values,
            CancellationToken cancellationToken = default)
            => await BaseStream.WriteAsync(values, cancellationToken);
    }
}
