# BinaryData

When parsing or storing data of binary file formats or protocols, the functionality offered by the default .NET
`BinaryReader` and `BinaryWriter` classes is often insufficient. It lacks support for a different byte order than
the system one, and cannot parse specific string or date formats (most prominently, 0-terminated strings instead of the
default variable-length prefixed .NET strings).

Further, navigating in binary files is slightly tedious when it is required to skip to another chunk in the file and
then navigate back. Also, aligning to specific block sizes might be a common task.

The `Syroot.BinaryData` library adds all this and more functionality by offering a large set of extension methods to
`Stream` instances. A `BinaryStream` class is provided, usable like a combination of the default .NET
`BinaryReader` and `BinaryWriter` classes, so that new functionality can be added almost instantly to existing
projects.

The upcoming `Syroot.BinaryData.Memory` library can work directly and performantly with in-memory data thanks to the
new .NET `Span<T>` functionality, without the need for `Stream` instances anymore.

The `Syroot.BinaryData.Serialization` library can serialize and deserialize complete class hierarchies without any
further code required.

## Installation

The libraries are available as NuGet packages:

- [Syroot.BinaryData](https://www.nuget.org/packages/Syroot.BinaryData) - Core functionality
- Syroot.BinaryData.Memory - Span functionality (currently experimental, not yet on NuGet)
- [Syroot.BinaryData.Serialization](https://www.nuget.org/packages/Syroot.BinaryData.Serialization) - Serialization
  functionality (adds the core package dependency)

If you want to compile the libraries yourself, Visual Studio 2017 with the 15.7 update is required, as the library uses
the latest C# 7.3 features not available in earlier versions.

### Deprecating 4.x and signed packages

The previous `Syroot.IO.BinaryData` package has been split into the core and the serialization packages mentioned
above and is now deprecated. The old packages will be unlisted in May 2019. Beyond many new features, several breaking
changes were made. Please have a look at the most recent [release notes](https://gitlab.com/Syroot/BinaryData/tags) for
more info.

Signed packages have also been unlisted as this legacy scenario is no longer supported by new versions.

Any unlisted packages can still be installed if referenced directly in a project.

## Documentation

- A code-oriented feature tour is available [on the wiki](https://gitlab.com/Syroot/BinaryData/wikis/home).
- Complete MSDN-style API documentation is hosted on [docs.syroot.com](https://docs.syroot.com/api-binarydata).
